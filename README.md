# Jekyll GitLab Metadata

Jekyll plugin to populate `site.gitlab` with metadata for GitLab Pages.

## Installation

First add `jekyll-gitlab-metadata` to your `Gemfile` as follows:

```ruby
gem "jekyll-gitlab-metadata"
```

Then run `bundler` from command line (or your IDE) to install the gem and update your `Gemfile.lock` e.g.
 
```bash
bundle
```

Finally, in your Jekyll config (`_config.yml`) add:

```yaml
plugins:
  - "jekyll-gitlab-metadata"
```

## Usage

The injected metadata is all namespaced in `site.gitlab`.

ex. A "Fork on GitLab" link could be added to a Liquid template as follows:

```liquid
<a href="{{ site.gitlab.repository_url }}">
	Fork on GitLab
</a>
```

## What metadata is available?

| metadata | description |
|---|---|
| environment | `JEKYLL_ENV` environment variable. Typically `"development"` or `"production"`. |
| pages_env | Alias for `environment` |
| hostname | `"gitlab.com"` by default. Can be overridden by the `GITLAB_HOST` environment variable. |
| pages_hostname | `"gitlab.io"` by default. Can be overridden by the `GITLAB_PAGES_HOST` environment variable. |
| api_url | GitLab instance API base URL e.g. `"https://gitlab.com/api/v4"` |
| help_url | GitLab instance help URL |
| project_title | Project title |
| owner_name | Project user/group |
| owner_url | User/group GitLab URL |
| repository_url | Project GitLab URL |
| repository_nwo | Namespace project i.e. `"<owner_name>/<repository_name"` |
| repository_name | Repository name |
| zip_url | URL to download the archived source for the GitLab Page as a zip file. |
| tar_url | URL to download the archived source for the GitLab Page as a tarball (`.tar.gz`). |
| clone_url | Git URL to clone the GitLab Page's repository. Authentication may be required. |
| releases_url | URL to the project's GitLab tags page. |
| issues_url | URL to the project's GitLab issues page |
| wiki_url | URL to the project's GitLab wiki page |
| is_user_page | `true` if this is a page for a GitLab group or user, otherwise `false`. |
| is_project_page | Opposite of `is_user_page` |
| show_downloads | `true` by default. Can be overridden by setting the `GITLAB_PAGES_SHOW_DOWNLOADS` environment variable to `false`/`no`. |
| url | Base URL of the published GitLab Pages i.e. `"https://<owner_name>.<pages_hostname>/<repository_name>"`. The `https` protocol can be overridden by the `GITLAB_PAGES_PROTOCOL` environment variable. |
| baseurl | Base path of the published GitLab Pages i.e. `"/<repository_name>"` |
| private | `true` if the GitLab project is private, `false` otherwise. |
| build_revision | The Git commit identifier (SHA) |

Environment variables can be set in your project's ` .gitlab-ci.yml`.

e.g.

```yaml
variables:
  JEKYLL_ENV: production
```

## Github Interop

This gem provides equivalent functionality to what Github Pages provides by default (via [jekyll-github-metadata](https://github.com/jekyll/github-metadata)) in the `site.github` namespace.

In order to provide some level of interoperability, by default the metadata is also made available in the `site.github` namespace, in addition to `site.gitlab`.

This behavior can be disabled by setting the `GITLAB_PAGES_GITHUB_INTEROP` environment variable to `false`/`no` in your project's `.gitlab-ci.yml`.

At the time of writing, when compared to Github Pages, the following metadata is missing:

| metadata | status |
|---|---|
| public_repositories | Not implemented |
| organization_members | Not implemented |
| project_tagline | Returns an empty string. |
| owner_gravatar_url | Not implemented |
| language | Not implemented |
| contributors | Not implemented |
| releases | Not implemented |
| latest_release | Not implemented |
| license | Not implemented |
| source | Not implemented |
| versions | Not implemented |

A good portion of the "not implemented" functionality could be implemented in this gem by querying the GitLab API. However, at present this gem is extremely simple, and all the existing information is retrieved exclusively from GitLab CI runner environment variables.

## Contributing

This project is MIT licensed and contributions are both welcome and encouraged.

Simply fork the repo on GitLab and submit a [merge request](https://gitlab.com/glassechidna/jekyll-gitlab-metadata/merge_requests).
