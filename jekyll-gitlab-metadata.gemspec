lib = File.expand_path('../lib/', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'jekyll-gitlab-metadata/version'

Gem::Specification.new do |s|
  s.name = 'jekyll-gitlab-metadata'
  s.version = Jekyll::GitlabMetadata::VERSION

  s.authors = ['Benjamin Dobell']
  s.email = ['contact@glassechidna.com.au']
  s.description = 'Jekyll plugin for injecting the site.gitlab namespace when built with GitLab CI'
  s.license = 'MIT'

  s.homepage = 'https://gitlab.com/glassechidna/jekyll-gitlab-metadata'
  s.summary = 'Jekyll plugin for injecting site.gitlab'

  s.files = `git ls-files -z`.split("\x0").grep(%r{^lib/})

  s.add_runtime_dependency 'jekyll', '~> 3.6'

  s.add_development_dependency 'bundler', '~> 1.16'
end
