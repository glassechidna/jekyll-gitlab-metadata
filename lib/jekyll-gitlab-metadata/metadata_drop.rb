require 'uri'

require 'jekyll'

module Jekyll
  module GitlabMetadata
    class MetadataDrop < Jekyll::Drops::Drop
      def initialize
        super(nil)

        if ENV['CI_REPOSITORY_URL']
          clone_uri = URI(ENV['CI_REPOSITORY_URL'])
          clone_uri.password = nil
          clone_uri.user = nil

          @clone_url = clone_uri.to_s.freeze
        else
          @clone_url = ''
        end
      end

      def environment
        Jekyll.env
      end

      def pages_env
        environment
      end

      def hostname
        ENV['GITLAB_HOST'] || 'gitlab.com'
      end

      def pages_hostname
        ENV['GITLAB_PAGES_HOST'] || 'gitlab.io'
      end

      def api_url
        "https://#{hostname}/api/v4"
      end

      def help_url
        "https://#{hostname}/help"
      end

      def public_repositories
        raise NotImplementedError
      end

      def organization_members
        raise NotImplementedError
      end

      def project_title
        ENV['CI_PROJECT_NAME'] || ''
      end

      def project_tagline
        '' # raise NotImplementedError
      end

      def owner_name
        ENV['CI_PROJECT_NAMESPACE'] || ''
      end

      def owner_url
        "https://#{hostname}/#{owner_name}"
      end

      def owner_gravatar_url
        raise NotImplementedError
      end

      def repository_url
        ENV['CI_PROJECT_URL'] || ''
      end

      def repository_nwo
        ENV['CI_PROJECT_PATH'] || ''
      end

      def repository_name
        repository_nwo.split('/').last
      end

      def zip_url
        "#{typeless_archive_url}.zip"
      end

      def tar_url
        "#{typeless_archive_url}.tar.gz"
      end

      def clone_url
        @clone_url
      end

      def releases_url
        "#{repository_url}/tags"
      end

      def issues_url
        "#{repository_url}/issues"
      end

      def wiki_url
        "#{repository_url}/wiki"
      end

      def language
        raise NotImplementedError
      end

      def is_user_page
        repository_name == "#{pages_hostname}/#{owner_name}"
      end

      def is_project_page
        !is_user_page
      end

      def show_downloads
        ['true', 'yes'].include?((ENV['GITLAB_PAGES_SHOW_DOWNLOADS'] || 'true').downcase)
      end

      def url
        "#{pages_protocol}://#{owner_name}.#{pages_hostname}#{baseurl}"
      end

      def baseurl
        "/#{repository_name}"
      end

      def contributors
        raise NotImplementedError
      end

      def releases
        raise NotImplementedError
      end

      def latest_release
        raise NotImplementedError
      end

      def private
        ENV['CI_PROJECT_VISIBILITY'] == 'private'
      end

      def license
        raise NotImplementedError
      end

      def source
        raise NotImplementedError
      end

      def versions
        raise NotImplementedError
      end

      def build_revision
        ENV['CI_COMMIT_SHA'] || ''
      end

      private

      def fallback_data
        {}
      end

      def typeless_archive_url
        "#{repository_url}/-/archive/#{build_revision}/#{repository_name}-#{build_revision}"
      end

      def pages_protocol
        ENV['GITLAB_PAGES_PROTOCOL'] || 'https'
      end
    end
  end
end
