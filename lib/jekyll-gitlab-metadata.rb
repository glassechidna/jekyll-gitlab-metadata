require_relative 'jekyll-gitlab-metadata/metadata_drop'

github_interop = ['true', 'yes'].include?((ENV['GITLAB_PAGES_GITHUB_INTEROP'] || 'true').downcase)

Jekyll::Hooks.register :site, :after_init do |site|
  site.config['gitlab'] = Jekyll::GitlabMetadata::MetadataDrop.new
  site.config['github'] = site.config['gitlab'] if github_interop
end
